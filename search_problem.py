import random
import numpy as np
from queue import PriorityQueue
import matplotlib.pyplot as plt


# Dummy Classes to Show the Algorithms are Correct #
class DummyNode(object):
    def __init__(self, value):
        self.state = DummyState(value)
        self.depth = 0
        self.children = []
        self.parent = None

    def __lt__(self, other):
        return self.state < other.state

    def __str__(self):
        return str(self.state)

    def expand(self):
        print(self.state, end="")
        return self.children

    def add_child(self, child_node):
        child_node.parent = self
        child_node.depth = self.depth + 1
        self.children.append(child_node)


class DummyState(object):
    def __init__(self, value):
        self.value = value

    def __eq__(self, other):
        if isinstance(other, DummyState):
            return self.value == other.value
        return False

    def __lt__(self, other):
        return self.value < other.value

    def __str__(self):
        return str(self.value)

    def is_goal(self, goal_state):
        return self.value == goal_state.value


def dummy_correct_heuristic(state, goal_state):
    return ord(goal_state.value) - ord(state.value)


def create_dummy_tree():
    tree_root = DummyNode("A")

    b = DummyNode("B")
    c = DummyNode("C")
    d = DummyNode("D")
    tree_root.add_child(b)
    tree_root.add_child(c)
    tree_root.add_child(d)

    b.add_child(DummyNode("E"))
    b.add_child(DummyNode("F"))

    c.add_child(DummyNode("G"))
    c.add_child(DummyNode("H"))

    d.add_child(DummyNode("I"))
    d.add_child(DummyNode("J"))

    return tree_root


# Relevant Object Classes to model Domain #

class Node(object):
    def __init__(self, state):
        assert(isinstance(state, BoardState))
        self.state = state
        self.depth = 0
        self.parent = None

    def __lt__(self, other):
        return self.state < other.state

    def __str__(self):
        return str(self.state)

    def expand(self):
        # Randomly choose a move and add child if valid.
        possible_moves = random.sample(ALLOWED_MOVES, len(ALLOWED_MOVES))
        child_moves = []
        for move in possible_moves:
            new_state = self.state.perform_move(move)
            if new_state is not None:
                child = Node(new_state)
                child.depth = self.depth + 1
                child_moves.append(child)
        return child_moves


class BoardState(object):
    def __init__(self, block_configuration, agent_position):
        self.agent_position = agent_position
        self.blocks = block_configuration

    def __eq__(self, other):
        if isinstance(other, BoardState):
            return self.same_block_position(other) and self.agent_position == other.agent_position
        return False

    def __lt__(self, other):
        return self.agent_position < other.agent_position

    def __str__(self):
        return "Agent: {}".format(self.agent_position)

    def same_block_position(self, other):
        for index, block in enumerate(self.blocks):
            if block != other.blocks[index]:
                return False
        return True

    def is_goal(self, goal_state):
        return self.same_block_position(goal_state)

    def perform_move(self, move):
        new_board = None
        if self.valid_move(move):
            new_block_config = []
            agent_j_pos = self.agent_position[0] + move[0]
            agent_i_pos = self.agent_position[1] + move[1]
            for block in self.blocks:
                new_block = block
                if new_block[0] == agent_j_pos and new_block[1] == agent_i_pos:
                    new_block = (self.agent_position[0], self.agent_position[1])
                new_block_config.append(new_block)
            new_board = BoardState(new_block_config, (agent_j_pos, agent_i_pos))

        return new_board

    def valid_move(self, move):
        agent_row = self.agent_position[0] + move[0]
        agent_col = self.agent_position[1] + move[1]
        return 0 <= agent_row < BOARD_SIZE and 0 <= agent_col < BOARD_SIZE


# Function Definitions #

def depth_first_search(start_node, goal_state):
    stack = [start_node]  # Fringe LIFO order
    nodes_expanded = 0
    while len(stack) > 0:
        elem = stack.pop()
        nodes_expanded += 1
        if elem.state.is_goal(goal_state):
            return elem, nodes_expanded
        else:
            # Add the children to the stack in reverse order so we always choose the leftmost branch first.
            children = elem.expand()
            for child in children:
                child.parent = elem
            stack.extend(reversed(children))
    return None, nodes_expanded


def depth_limited_search(start_node, goal_state, max_depth):
    solution = [None] * (max_depth + 1)  # Solution should be shorter than max_depth allowed.
    stack = [start_node]  # Nodes to go through in LIFO order. -> Fringe
    nodes_expanded = 0
    while len(stack) > 0:
        elem = stack.pop()
        if elem.depth < len(solution):
            # Add current element to solution and clear old paths.
            solution[elem.depth] = elem
            solution[elem.depth+1:] = [None] * (max_depth - elem.depth)
        if elem.depth <= max_depth:
            nodes_expanded += 1
            if elem.state.is_goal(goal_state):
                return solution, nodes_expanded
            else:
                # Add the children to the stack in reverse order so we always choose the leftmost branch first.
                stack.extend(reversed(elem.expand()))
    return None, nodes_expanded


def iterative_deepening_search(start_node, goal_state):
    current_depth = 0
    total_nodes_expanded = 0
    while current_depth <= MAX_POSSIBLE_TREE_DEPTH:
        solution, expanded = depth_limited_search(start_node, goal_state, current_depth)
        total_nodes_expanded += expanded
        if solution is not None:
            return solution, total_nodes_expanded
        current_depth += 1
    return None, total_nodes_expanded


def breadth_first_search(start_node, goal_state):
    queue = [start_node]  # Queue elements and paths originating from them (possible solutions) -> FIFO Fringe
    nodes_expanded = 0
    while len(queue) > 0:
        elem = queue.pop(0)
        nodes_expanded += 1
        if elem.state.is_goal(goal_state):
            return elem, nodes_expanded
        else:
            # Add each child and the path to it to the queue for further exploration
            for child in elem.expand():
                child.parent = elem
                queue.append(child)
    return None, nodes_expanded


def heuristic_puzzle(state, goal_state):
    accumulated_distance = 0
    for index, block in enumerate(state.blocks):
        accumulated_distance += abs(goal_state.blocks[index][0] - block[0])  # Row distance
        accumulated_distance += abs(goal_state.blocks[index][1] - block[1])  # Column distance
    return accumulated_distance


def a_star_search(start_node, goal_state, heuristic_function):
    priority_queue = PriorityQueue()  # Fringe
    priority_queue.put((0, start_node))
    nodes_expanded = 0
    while not priority_queue.empty():
        elem = priority_queue.get()
        nodes_expanded += 1
        cost_so_far = elem[0]
        elem = elem[1]  # Get the Node
        if elem.state.is_goal(goal_state):
            return elem, nodes_expanded
        else:
            children = elem.expand()
            for child in children:
                child.parent = elem
                # elem.depth is the accumulated cost of getting to here (current depth)
                elem_priority = cost_so_far + heuristic_function(child.state, goal_state)
                priority_queue.put((elem_priority, child))
    return None, nodes_expanded


def pretty_print_solution(node):
    path = []
    while node.parent is not None:
        path.append(node)
        node = node.parent
    path.append(node)
    path = list(reversed([str(s) for s in path]))
    print("Solution path: {}".format(path), "\n")


def solution_length(node):
    length = 0
    while node.parent is not None:
        length += 1
        node = node.parent
    return length


MAX_POSSIBLE_TREE_DEPTH = 50  # This means 'infinity' for ids.

'''
Show correct algorithm implementation

Initial tree is:

          A 
    B     C     D
   E F   G H   I J
   
'''
example_tree = create_dummy_tree()

# BFS example
print('BFS Example Run')
bfs, _ = breadth_first_search(example_tree, DummyState("I"))
print("")
pretty_print_solution(bfs)

# DFS example
print('DFS Example Run')
dfs, _ = depth_first_search(example_tree, DummyState("I"))
print("")
pretty_print_solution(dfs)

# IDS example
print('IDS Example Run')
ids, _ = iterative_deepening_search(example_tree, DummyState("I"))
print("")
print("Solution path:", [str(s) for s in ids], "\n")

# A* example
print('A* Example Run')
a_star, _ = a_star_search(example_tree, DummyState("I"), dummy_correct_heuristic)
print("")
pretty_print_solution(a_star)

'''
    Run the algorithms for the real problem
'''

# Problem Defining Variables #
# (ROW, COLUMN) #

BOARD_SIZE = 4
ALLOWED_MOVES = [(1, 0), (-1, 0), (0, 1), (0, -1)]  # Agent can move 1 unit Down - Up - Right - Left
BLOCK_A_START = 0
GOAL_STATE_BLOCKS = [
    (BLOCK_A_START + 2, BLOCK_A_START + 1),  # A
    (BLOCK_A_START + 1, BLOCK_A_START + 1),  # B
    (BLOCK_A_START, BLOCK_A_START + 1)       # C
]
goal_state_board = BoardState(GOAL_STATE_BLOCKS, (BLOCK_A_START, BOARD_SIZE - 1))

difficulties = []

# 1 move away from solution
blocks_one = [
        (BLOCK_A_START + 2, BLOCK_A_START),      # A
        (BLOCK_A_START + 1, BLOCK_A_START + 1),  # B
        (BLOCK_A_START, BLOCK_A_START + 1)       # C
]
agent_one = (BLOCK_A_START + 2, BLOCK_A_START + 1)
difficulties.append((blocks_one, agent_one))

# 2 moves away from solution
agent_two = (BLOCK_A_START + 2, BLOCK_A_START + 2)
difficulties.append((blocks_one, agent_two))

# 3 moves away from solution
agent_three = (BLOCK_A_START + 2, BLOCK_A_START + 3)
difficulties.append((blocks_one, agent_three))

# 4 moves away from solution
agent_four = (BLOCK_A_START + 3, BLOCK_A_START + 3)
difficulties.append((blocks_one, agent_four))

# 5 moves away from solution
agent_five = (BLOCK_A_START, BLOCK_A_START + 3)
difficulties.append((blocks_one, agent_five))

# 6 moves away
blocks_six = [
        (BLOCK_A_START + 2, BLOCK_A_START),      # A
        (BLOCK_A_START + 1, BLOCK_A_START + 1),  # B
        (BLOCK_A_START, BLOCK_A_START + 2)       # C
]
agent_six = (BLOCK_A_START, BLOCK_A_START)
difficulties.append((blocks_six, agent_six))

# 7 moves away
agent_seven = (BLOCK_A_START + 1, BLOCK_A_START)
difficulties.append((blocks_six, agent_seven))

# 8 moves away
blocks_eight = [
        (BLOCK_A_START + 2, BLOCK_A_START),  # A
        (BLOCK_A_START + 1, BLOCK_A_START),  # B
        (BLOCK_A_START, BLOCK_A_START + 2)   # C
]
agent_eight = (BLOCK_A_START + 1, BLOCK_A_START + 1)
difficulties.append((blocks_eight, agent_eight))

nodes_expanded_difficulty = {
    'bfs': [[] for _ in range(len(difficulties))],
    'dfs': [[] for _ in range(len(difficulties))],
    'ids': [[] for _ in range(len(difficulties))],
    'star': [[] for _ in range(len(difficulties))]
}

for _ in range(10):
    for idx, (blocks, agent) in enumerate(difficulties):
        initial_board = BoardState(blocks, agent)
        _, node_count = breadth_first_search(Node(initial_board), goal_state_board)

        nodes_expanded_difficulty['bfs'][idx].append(node_count)
        _, node_count = depth_first_search(Node(initial_board), goal_state_board)

        nodes_expanded_difficulty['dfs'][idx].append(node_count)
        _, node_count = iterative_deepening_search(Node(initial_board), goal_state_board)

        nodes_expanded_difficulty['ids'][idx].append(node_count)
        _, node_count = a_star_search(Node(initial_board), goal_state_board, heuristic_puzzle)
        nodes_expanded_difficulty['star'][idx].append(node_count)

titles = {
    'bfs': 'Breadth-First Search',
    'dfs': 'Depth-First Search',
    'ids': 'Iterative Deepening Search',
    'star': 'A* Search'
}

legends = ['{} Moves'.format(i + 1) for i in range(len(difficulties))]

# Plot the effect of expansion randomness for each search
fig, ax = plt.subplots(nrows=1, ncols=4)
for plot_number, key in enumerate(nodes_expanded_difficulty.keys()):
    for idx in range(len(difficulties)):
        ax[plot_number].set_title(titles[key])
        ax[plot_number].plot(range(10), nodes_expanded_difficulty[key][idx], label=legends[idx])
plt.legend()
fig.suptitle("Effect of Random node creation for each run")
plt.show()

# Plot average nodes expanded for each run
x_labels = [(x+1) for x in range(len(difficulties))]
for key in nodes_expanded_difficulty.keys():
    means = [np.mean(e) for e in nodes_expanded_difficulty[key]]
    plt.plot(x_labels, means, label=titles[key],  marker='o')
plt.legend()
plt.title("Average nodes expanded for each difficulty")
plt.show()

# Real problem space
blocks_real = [
        (BLOCK_A_START, BLOCK_A_START),      # A
        (BLOCK_A_START, BLOCK_A_START + 1),  # B
        (BLOCK_A_START, BLOCK_A_START + 2)   # C
]
real_agent_position = (BLOCK_A_START, BOARD_SIZE - 1)
initial_node = Node(BoardState(blocks_real, real_agent_position))

# Will end in reasonable time
real_a_star_solution, star_nodes_expanded = a_star_search(initial_node, goal_state_board, heuristic_puzzle)
print("Real Problem Solution:")
print("")
print("A*")
print("Nodes expanded: {}".format(star_nodes_expanded))
print("Solution Depth: {}".format(solution_length(real_a_star_solution)))
pretty_print_solution(real_a_star_solution)

real_ids_solution, ids_nodes_expanded = iterative_deepening_search(initial_node, goal_state_board)
print("IDS")
print("Nodes expanded: {}".format(ids_nodes_expanded))
print("Solution Depth: {}".format(len(real_ids_solution) - 1))
print("Solution path:", [str(s) for s in real_ids_solution])

# Not guaranteed to end

real_bfs_solution, bfs_nodes_expanded = breadth_first_search(initial_node, goal_state_board)
print("BFS")
print("Nodes expanded: {}".format(bfs_nodes_expanded))
print("Solution Depth: {}".format(solution_length(real_bfs_solution)))
pretty_print_solution(real_bfs_solution)

# Most likely not going to end
real_dfs_solution, dfs_nodes_expanded = depth_first_search(initial_node, goal_state_board)
print("DFS")
print("Nodes expanded: {}".format(dfs_nodes_expanded))
print("Solution Depth: {}".format(solution_length(real_dfs_solution)))
pretty_print_solution(real_dfs_solution)
